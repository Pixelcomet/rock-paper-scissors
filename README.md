# RockPaperScissors

## Before you do anything else: run `npm i`

## Deploying the smart contract

in `./contract/`:  
Local (Ganache): `truffle migrate --network development`  
Local re-deploy: `truffle migrate --reset`  
Ropsten: `truffle migrate --network ropsten` (make sure to update the mnemonic and infura url in `truffle-config.js`)

## Development server

Install the angular cli: `npm i -g @angular/cli`.

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files. **When testing locally: Make sure to use ganache and configure your web3 provider accordingly.**

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

**Edit `./src/environments/environment.ts` and/or `./src/environments/environment.prod.ts` to reflect your contract**

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

To get help with all the Ethereum stuff, visit [Truffle Suite](https://www.trufflesuite.com) and [the Solidity Docs](https://solidity.readthedocs.io/en/latest/) (developed with: v0.6.11).
