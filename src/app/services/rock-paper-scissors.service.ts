import { Injectable, EventEmitter } from '@angular/core';
import * as md5 from 'md5';
import { Game } from '../interfaces/Game';
import { ResolvedGame } from '../interfaces/ResolvedGame';
const Web3 = require('web3');
const TruffleContract = require('truffle-contract');

const contractAbi: any = require('../../../contract/build/contracts/RockPaperScissors.json');

import BigNumber from 'bignumber.js';
import { environment } from 'src/environments/environment';

declare let require: any;
declare let window: any;

const weiInEth = 1000000000000000000;

@Injectable({
    providedIn: 'root',
})
export class rps {
    private account: any = null;
    private readonly web3: any;
    private executableContract: any;
    public ready = false;

    gasPrice: any;
    gasPriceHex: number;
    gasLimitHex: number;

    currentBlockNumber = 0;

    playerRegisteredEvent: EventEmitter<string>;
    playerNameChangedEvent: EventEmitter<string>;
    newGameEvent: EventEmitter<string>;
    challengeWithdrawnEvent: EventEmitter<string>;
    challengeAcceptedEvent: EventEmitter<string>;
    challengedMovedEvent: EventEmitter<string>;
    defaultEventEmitter: EventEmitter<string>;
    resolvedGameEvent: EventEmitter<ResolvedGame>;

    constructor() {
        this.playerRegisteredEvent = new EventEmitter();
        this.playerNameChangedEvent = new EventEmitter<string>();
        this.newGameEvent = new EventEmitter<string>();
        this.challengeWithdrawnEvent = new EventEmitter<string>();
        this.challengeAcceptedEvent = new EventEmitter<string>();
        this.challengedMovedEvent = new EventEmitter<string>();
        this.defaultEventEmitter = new EventEmitter<string>();
        this.resolvedGameEvent = new EventEmitter<ResolvedGame>();

        if (window.ethereum === undefined) {
            alert('Non-Ethereum browser detected. Install MetaMask');
        } else {
            if (typeof window.web3 !== 'undefined') {
                this.web3 = window.web3.currentProvider;
            } else {
                this.web3 = new Web3.providers.HttpProvider(
                    'http://localhost:8545'
                );
            }
            window.web3 = new Web3(window.ethereum);

            window.ethereum.enable();
            window.ethereum.on('accountsChanged', () => {
                location.reload();
            });
        }

        this.init();
    }

    wait = (ms) => new Promise((r) => setTimeout(r, ms));

    ethToWei = (eth) => eth * weiInEth;
    weiToEth = (wei) =>
        parseFloat((wei * (1 / weiInEth)).toFixed(5).toString());

    async init() {
        const paymentContract = TruffleContract(contractAbi);
        paymentContract.setProvider(this.web3);
        paymentContract.at(environment.contractAddress);

        this.executableContract = await paymentContract
            .deployed()
            .then((instance) => {
                return instance;
            })
            .catch((error) => {
                console.log(error);
            });

        this.executableContract.NewPlayer().on('data', (event) => {
            if (this.account === event.args.atAddress) {
                this.playerRegisteredEvent.emit();
            }
        });

        this.executableContract.PlayerNameChanged().on('data', (event) => {
            console.log(event);

            if (this.account === event.returnValues.atAddress) {
                this.playerNameChangedEvent.emit(event.returnValues.name);
            }
        });

        this.executableContract.NewGame().on('data', (event) => {
            if (
                this.account === event.returnValues.challenger ||
                this.account === event.returnValues.challenged
            ) {
                this.newGameEvent.emit(event.returnValues.gameId);
            }
        });

        this.executableContract.ChallengeWithdrawn().on('data', (event) => {
            this.challengeWithdrawnEvent.emit(event.returnValues.gameId);
        });

        this.executableContract.ChallengeAccepted().on('data', (event) => {
            this.challengeAcceptedEvent.emit(event.returnValues.gameId);
        });

        this.executableContract.ChallengedMoved().on('data', (event) => {
            this.challengedMovedEvent.emit(event.returnValues.gameId);
        });

        this.executableContract.Default().on('data', (event) => {
            this.defaultEventEmitter.emit(event.returnValues.gameId);
        });

        this.executableContract.GameResolved().on('data', async (event) => {
            console.log(event);

            this.resolvedGameEvent.emit({
                gameId: event.returnValues.gameId,
                bet: this.weiToEth(event.returnValues.bet),
                challenger: event.returnValues.challenger,
                challengerName: await this.getDisplayNameFromAddress(
                    event.returnValues.challenger
                ),
                challengerAddressHash: md5(event.returnValues.challenger),
                challenged: event.returnValues.challenged,
                challengedName: await this.getDisplayNameFromAddress(
                    event.returnValues.challenged
                ),
                challengedAddressHash: md5(event.returnValues.challenged),
                challengerMove: event.returnValues.challengerMove,
                challengedMove: event.returnValues.challengedMove,
            });
        });

        this.ready = true;

        this.pollBlockNumber();
    }

    async pollBlockNumber() {
        await this.ensureReady();
        while (true) {
            this.currentBlockNumber = await window.web3.eth.getBlockNumber();
            await this.wait(3000);
        }
    }

    async getAccount(): Promise<any> {
        if (this.account == null) {
            this.account = (await new Promise((resolve, reject) => {
                console.log(window.web3.eth);
                window.web3.eth.getAccounts((err, retAccount) => {
                    if (retAccount.length > 0) {
                        this.account = retAccount[0];
                        resolve(this.account);
                    } else {
                        reject('No accounts found.');
                    }
                    if (err != null) {
                        reject('Error retrieving account');
                    }
                });
            })) as Promise<any>;
        }
        return Promise.resolve(this.account);
    }

    public async getUserBalance(): Promise<any> {
        const account = await this.getAccount();
        return new Promise((resolve, reject) => {
            window.web3.eth.getBalance(account, (err, balance) => {
                if (!err) {
                    const retVal = {
                        account,
                        balance,
                    };
                    resolve(retVal);
                } else {
                    reject({ account: 'error', balance: 0 });
                }
            });
        }) as Promise<any>;
    }

    async ensureReady() {
        while (!this.ready) {
            await this.wait(10);
        }
    }

    async register(name) {
        await this.ensureReady();

        this.executableContract.register(name, {
            from: await this.getAccount(),
        });
    }

    async changeName(newName) {
        await this.ensureReady();

        this.executableContract.changeName(newName, {
            from: await this.getAccount(),
        });
    }

    async getDisplayName() {
        await this.ensureReady();

        return await this.executableContract.getDisplayName({
            from: await this.getAccount(),
        });
    }

    async getDisplayNameFromAddress(address) {
        await this.ensureReady();

        return await this.executableContract.getDisplayNameFromAddress(address);
    }

    async playerIsRegistered(address) {
        await this.ensureReady();

        return await this.executableContract.playerIsRegistered(address);
    }

    async getOwner(): Promise<string> {
        await this.ensureReady();

        return await this.executableContract.getOwner();
    }

    async getFees(): Promise<number> {
        await this.ensureReady();

        console.log(await this.executableContract.getFees());

        return this.weiToEth(await this.executableContract.getFees());
    }

    async payoutFees() {
        await this.ensureReady();

        return await this.executableContract.payoutFees({
            from: await this.getAccount(),
        });
    }

    async createGame(bet, address, yourMoveHash) {
        await this.ensureReady();

        return await this.executableContract.createGame(address, yourMoveHash, {
            from: await this.getAccount(),
            value: bet * weiInEth,
        });
    }

    async getNumberOfChallenges(forAddress) {
        await this.ensureReady();

        return (
            await this.executableContract.getNumberOfChallenges(forAddress)
        ).toNumber();
    }

    async getGameIdForChallengeWithNumber(
        numberOfChallenge,
        forAddress: string
    ) {
        await this.ensureReady();

        return await this.executableContract.getGameIdForChallengeWithNumber(
            numberOfChallenge,
            forAddress
        );
    }

    async getGameForAddress(address: string, atIndex: number): Promise<Game> {
        const rawGame = await this.executableContract.getGameForAddress(
            address,
            atIndex
        );
        const game = await this.parseRawGame(rawGame);
        game.id = rawGame.gameId;

        return game;
    }

    async getGameById(gameId): Promise<Game> {
        await this.ensureReady();

        const rawGame = await this.executableContract.getGameById(gameId);
        const game = await this.parseRawGame(rawGame);
        game.id = gameId;

        return game;
    }

    async parseRawGame(rawGame: any) {
        const game: Game = {
            id: '',
            challenger: '',
            challengerName: '',
            challengerAddressHash: '',
            challenged: '',
            challengedName: '',
            challengedAddressHash: '',
            bet: 0,
            accepted: false,
            challengedMove: '',
            blockNumLastAction: 0,
            yourMove: '',
            salt: '',
        };

        game.challenger = rawGame.challenger;
        game.challengerName = await this.getDisplayNameFromAddress(
            game.challenger
        );
        game.challengerAddressHash = md5(game.challenger);
        game.challenged = rawGame.challenged;
        game.challengedName = await this.getDisplayNameFromAddress(
            game.challenged
        );
        game.challengedAddressHash = md5(game.challenged);
        game.bet = new BigNumber(rawGame.bet).dividedBy(weiInEth).toNumber();
        game.accepted = rawGame.accepted;
        game.challengedMove = rawGame.challengedMove;
        game.blockNumLastAction = parseInt(rawGame.blockNumLastAction, 10);

        return game;
    }

    async getGames(forAddress) {
        const numberOfChallenges = await this.getNumberOfChallenges(forAddress);
        const games: Game[] = [];

        for (let i = 0; i < numberOfChallenges; i++) {
            games.push(await this.getGameForAddress(forAddress, i));
        }

        return games;
    }

    async acceptChallenge(game) {
        await this.ensureReady();

        return await this.executableContract.acceptChallenge(game.id, {
            from: await this.getAccount(),
            value: game.bet * weiInEth,
        });
    }

    async makeCounterMove(gameId, yourMove) {
        await this.ensureReady();

        return await this.executableContract.makeCounterMove(gameId, yourMove, {
            from: await this.getAccount(),
        });
    }

    async challengerDefaults(
        gameId,
        indexInChallengerList,
        indexInChallengedList
    ) {
        return await this.executableContract.challengerDefaults(
            gameId,
            indexInChallengerList,
            indexInChallengedList,
            {
                from: await this.getAccount(),
            }
        );
    }
    async challengedDefaults(
        gameId,
        indexInChallengerList,
        indexInChallengedList
    ) {
        return await this.executableContract.challengedDefaults(
            gameId,
            indexInChallengerList,
            indexInChallengedList,
            {
                from: await this.getAccount(),
            }
        );
    }

    async resolveChallenge(
        gameId,
        yourMoveWithSalt,
        indexInChallengerList,
        indexInChallengedList
    ) {
        return await this.executableContract.resolveChallenge(
            gameId,
            yourMoveWithSalt,
            indexInChallengerList,
            indexInChallengedList,
            {
                from: await this.getAccount(),
            }
        );
    }

    async withdrawChallenge(
        gameId,
        indexInChallengerList,
        indexInChallengedList
    ) {
        return await this.executableContract.withdrawChallenge(
            gameId,
            indexInChallengerList,
            indexInChallengedList,
            {
                from: await this.getAccount(),
            }
        );
    }

    async computeGameId(challenger, challenged, bet, moveHash) {
        bet = this.ethToWei(bet);
        bet = '0x' + bet.toString(16);
        return await this.executableContract.computeGameId(
            challenger,
            challenged,
            bet,
            moveHash
        );
    }

    async findGameIndexForPlayer(
        playerAddress: string,
        gameId: string
    ): Promise<number> {
        const numberOfChallenges = await this.getNumberOfChallenges(
            playerAddress
        );

        for (let i = 0; i < numberOfChallenges; i++) {
            const id = await this.getGameIdForChallengeWithNumber(
                i,
                playerAddress
            );

            if (id === gameId) {
                return i;
            }
        }
    }
}
