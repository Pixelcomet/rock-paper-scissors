import { TestBed } from '@angular/core/testing';

import { rps } from './rock-paper-scissors.service';

describe('rps', () => {
    let service: rps;

    beforeEach(() => {
        TestBed.configureTestingModule({});
        service = TestBed.inject(rps);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });
});
