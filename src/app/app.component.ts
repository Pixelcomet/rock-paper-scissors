import { Component, OnInit, NgZone } from '@angular/core';
import { rps } from './services/rock-paper-scissors.service';
import keccack256 from 'keccak256';
import { Game } from './interfaces/Game';
import { ResolvedGame } from './interfaces/ResolvedGame';
import * as md5 from 'md5';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from 'src/environments/environment';

interface NewGame {
    bet: number;
    challengedAddress: string;
    yourMove: string;
    salt: string;
}

const newGameProto: NewGame = {
    bet: 0,
    challengedAddress: '',
    yourMove: 'r',
    salt: '',
};

interface AcceptedGame {
    gameId: string;
    yourMove: string;
}

interface Move {
    move: string;
    salt: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [],
})
export class AppComponent implements OnInit {
    myDisplayName: string;
    newName: 'Player 1';

    newGame: NewGame;

    games: Game[];

    index1: number;
    index2: number;

    identiconHash: string;
    playerRegistered = true;
    account: string;

    changeNameDialogOpen = false;

    updating = false;

    registerInProgress = false;

    backgroundImage: string;

    showSaltNewGame = false;
    showSaltReveal = false;

    resolvedGames: ResolvedGame[] = [];

    owner: string;
    fees: number;

    wait = (ms) => new Promise((r) => setTimeout(r, ms));

    constructor(
        public rps: rps,
        private snackBar: MatSnackBar,
        private ngZone: NgZone
    ) {
        this.owner = environment.owner;

        this.rps.playerRegisteredEvent.subscribe(() => {
            this.ngZone.run(() => {
                this.ngOnInit();
            });
        });

        this.rps.playerNameChangedEvent.subscribe((newName) => {
            this.ngZone.run(() => {
                this.myDisplayName = newName;
            });
        });

        this.rps.newGameEvent.subscribe((gameId) => {
            this.ngZone.run(async () => {
                const newGame = await this.rps.getGameById(gameId);
                if (newGame.bet > 0) {
                    this.games.push(newGame);
                }
            });
        });

        this.rps.challengeWithdrawnEvent.subscribe((gameId) => {
            this.ngZone.run(async () => {
                this.games = this.games.filter((g) => g.id !== gameId);
            });
        });

        this.rps.challengeAcceptedEvent.subscribe((gameId) => {
            this.ngZone.run(async () => {
                for (let i in this.games) {
                    if (this.games[i].id === gameId) {
                        this.games[i] = await this.rps.getGameById(gameId);
                    }
                }
            });
        });

        this.rps.challengedMovedEvent.subscribe((gameId) => {
            this.ngZone.run(async () => {
                for (let i in this.games) {
                    if (this.games[i].id === gameId) {
                        this.games[i] = await this.rps.getGameById(gameId);

                        if (this.games[i].challenger === this.account) {
                            this.games[i].yourMove = this.getMove(gameId).move;
                            this.games[i].salt = this.getMove(gameId).salt;
                        }
                    }
                }
            });
        });

        this.rps.defaultEventEmitter.subscribe((gameId) => {
            this.ngZone.run(async () => {
                for (let i in this.games) {
                    if (this.games[i].id === gameId) {
                        this.games.splice(+i, 1);
                    }
                }
            });
        });

        this.rps.resolvedGameEvent.subscribe((resolvedGame: ResolvedGame) => {
            console.log(resolvedGame);

            this.ngZone.run(async () => {
                this.resolvedGames.push(resolvedGame);
                this.games = this.games.filter(
                    (g) => g.id !== resolvedGame.gameId
                );
                this.updateFeeDisplay();
            });
        });

        this.backgroundImage = this.randomBackground();

        this.resetNewGame();
    }

    async ngOnInit() {
        this.updating = true;

        this.account = await this.rps.getAccount();
        this.updateFeeDisplay();
        this.playerRegistered = await this.rps.playerIsRegistered(this.account);
        await this.updateDisplayName();
        await this.updateGames();
        this.identiconHash = md5(this.account);

        this.updating = false;
    }

    async updateFeeDisplay() {
        if (this.account === this.owner) {
            this.fees = await this.rps.getFees();
        }
    }

    resetNewGame() {
        this.newGame = this.clone(newGameProto);
        this.newGame.salt = this.randomSalt();
    }

    storeMove(gameId: string, move: Move) {
        window.localStorage.setItem(gameId, JSON.stringify(move));
    }

    getMove(gameId: string): Move {
        return JSON.parse(window.localStorage.getItem(gameId));
    }

    removeMove(gameId: string) {
        window.localStorage.removeItem(gameId);
    }

    randomSalt(): string {
        return Math.floor(Math.random() * 10000000000000).toString(16);
    }

    randomBackground() {
        return `url('./assets/${Math.floor(Math.random() * 6)}.jpg')`;
    }

    register() {
        this.rps.register(this.newName);
        this.registerInProgress = true;
    }

    async changeName() {
        this.updating = true;
        await this.rps.changeName(this.newName);
        this.changeNameDialogOpen = false;
        this.updating = false;
    }

    async updateDisplayName() {
        this.myDisplayName = await this.rps.getDisplayName();
    }

    hash = (s: string) => `0x${keccack256(s).toString('hex')}`;
    clone = (obj: any) => JSON.parse(JSON.stringify(obj));

    async createGame() {
        const gameId = await this.rps.computeGameId(
            this.account,
            this.newGame.challengedAddress,
            this.newGame.bet,
            this.hash(this.newGame.yourMove + '_' + this.newGame.salt)
        );

        this.storeMove(gameId, {
            move: this.newGame.yourMove,
            salt: this.newGame.salt,
        });

        console.log(gameId);

        this.updating = true;
        await this.rps.createGame(
            this.newGame.bet,
            this.newGame.challengedAddress,
            this.hash(this.newGame.yourMove + '_' + this.newGame.salt)
        );
        this.updating = false;

        this.resetNewGame();
    }

    async updateGames() {
        const ownAddress = await this.rps.getAccount();
        this.games = await this.rps.getGames(ownAddress);

        for (const i in this.games) {
            if (
                this.games[i].challengedMove !== '' &&
                this.games[i].challenger === this.account
            ) {
                this.games[i].yourMove = this.getMove(this.games[i].id).move;
                this.games[i].salt = this.getMove(this.games[i].id).salt;
            }
        }
    }

    async acceptChallenge(game) {
        this.updating = true;
        await this.rps.acceptChallenge(game);
        this.updating = false;
    }

    async makeCounterMove(game) {
        this.updating = true;
        await this.rps.makeCounterMove(game.id, game.yourMove);
        this.updating = false;
    }

    async resolveChallenge(game) {
        this.updating = true;
        const indices = await this.gameIndices(game);

        await this.rps.resolveChallenge(
            game.id,
            game.yourMove + '_' + game.salt,
            indices[0],
            indices[1]
        );
        this.updating = false;
    }

    async gameIndices(game: Game): Promise<any> {
        const myIndex = this.games.findIndex((g) => g.id === game.id);
        const theirIndex = await this.rps.findGameIndexForPlayer(
            game.challenged,
            game.id
        );

        return [myIndex, theirIndex];
    }

    async withdraw(game: Game) {
        this.updating = true;
        const indices = await this.gameIndices(game);

        await this.rps.withdrawChallenge(game.id, indices[0], indices[1]);
        this.updating = false;
    }

    async challengerDefaults(game: Game) {
        this.updating = true;
        const indices = await this.gameIndices(game);

        await this.rps.challengerDefaults(game.id, indices[0], indices[1]);
        this.updating = false;
    }

    async challengedDefaults(game: Game) {
        this.updating = true;
        const indices = await this.gameIndices(game);

        await this.rps.challengedDefaults(game.id, indices[0], indices[1]);
        this.updating = false;
    }

    evalGame(p1: string, p2: string): number {
        if (p1 === p2) {
            return 2;
        }

        const concatenated = p1 + p2;

        if (concatenated === 'rp') {
            return 1;
        }
        if (concatenated === 'rs') {
            return 0;
        }
        if (concatenated === 'pr') {
            return 0;
        }
        if (concatenated === 'ps') {
            return 1;
        }
        if (concatenated === 'sr') {
            return 1;
        }
        if (concatenated === 'sp') {
            return 0;
        }
    }

    dismissResolvedGame(gameId: string) {
        this.resolvedGames = this.resolvedGames.filter(
            (g) => g.gameId !== gameId
        );
    }

    prefillForm(resolvedGame: ResolvedGame) {
        this.newGame.bet = resolvedGame.bet;
        this.newGame.challengedAddress =
            resolvedGame.challenger === this.account
                ? resolvedGame.challenged
                : resolvedGame.challenger;
        this.dismissResolvedGame(resolvedGame.gameId);
    }
}
