export interface ResolvedGame {
    gameId: string;
    bet: number;
    challenger: string;
    challengerName: string;
    challengerAddressHash: string;
    challenged: string;
    challengedName: string;
    challengedAddressHash: string;
    challengerMove: string;
    challengedMove: string;
}
