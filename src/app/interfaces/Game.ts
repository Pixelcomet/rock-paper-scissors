export interface Game {
    id: string;
    challenger: string;
    challengerName: string;
    challengerAddressHash: string;
    challenged: string;
    challengedName: string;
    challengedAddressHash: string;
    bet: number;
    accepted: boolean;
    challengedMove: string;
    blockNumLastAction: number;
    yourMove: string;
    salt: string;
}
