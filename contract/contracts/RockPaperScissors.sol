// SPDX-License-Identifier: UNLICENSED

pragma solidity >=0.4.21 <0.7.0;

contract RockPaperScissors {
    mapping(address => string) private players;
    mapping(bytes32 => Game) private games;
    mapping(address => bytes32[]) private challenges;

    address payable private owner;
    uint256 private fees;

    struct Game {
        address payable challenger;
        address payable challenged;
        uint256 bet;
        bool accepted;
        bytes32 challengerMoveHash;
        string challengedMove;
        uint256 blockNumLastAction;
    }

    // Events are used by the web interface to update once relevant changes
    // occur on the blockchain

    event NewPlayer(address payable atAddress, string name);
    event PlayerNameChanged(address payable atAddress, string name);
    event NewGame(
        address payable challenger,
        address payable challenged,
        bytes32 gameId
    );
    event ChallengeWithdrawn(bytes32 gameId);
    event ChallengeAccepted(bytes32 gameId);
    event ChallengedMoved(bytes32 gameId);
    event Default(bytes32 gameId);
    event GameResolved(
        bytes32 gameId,
        uint256 bet,
        address payable challenger,
        address payable challenged,
        string challengerMove,
        string challengedMove
    );

    modifier onlyRegisteredUsers {
        require(
            !stringsAreEqual(players[msg.sender], ""),
            "Please register first."
        );
        _;
    }

    // modifier-like functions make it possible to re-use requires, but don't
    // consume any of the 16 local variables you can have

    function onlyExistingGames(bytes32 gameId) private view {
        require(games[gameId].bet != 0, "Game does not exist");
    }

    function onlyChallenger(bytes32 gameId) private view {
        require(
            games[gameId].challenger == msg.sender,
            "Only the challenger can call this function."
        );
    }

    function onlyChallenged(bytes32 gameId) private view {
        require(
            games[gameId].challenged == msg.sender,
            "Only the challenged can call this function."
        );
    }

    function onlyAcceptedChallenges(bytes32 gameId) private view {
        require(
            games[gameId].blockNumLastAction != 0,
            "Challenge is not yet accepted"
        );
    }

    function onlyChallengesWithCounterMove(bytes32 gameId) private view {
        require(
            !stringsAreEqual(games[gameId].challengedMove, ""),
            "Challenge is not yet accepted"
        );
    }

    function onlyValidMoves(string memory move) private pure {
        require(
            stringsAreEqual(firstChar(move), "r") ||
                stringsAreEqual(firstChar(move), "p") ||
                stringsAreEqual(firstChar(move), "s"),
            "Invalid move."
        );
    }

    // checks if a game is at the specified indices in the challenges arrays
    // for both players
    function onlyCorrectIndices(
        bytes32 gameId,
        uint256 indexInChallengerList,
        uint256 indexInChallengedList
    ) private view {
        require(
            challenges[games[gameId].challenger][indexInChallengerList] !=
                bytes32(0x0),
            "Challenge not found."
        );
        require(
            challenges[games[gameId].challenged][indexInChallengedList] !=
                bytes32(0x0),
            "Challenge not found."
        );
        require(
            challenges[games[gameId].challenger][indexInChallengerList] ==
                gameId,
            "Wrong index."
        );
        require(
            challenges[games[gameId].challenged][indexInChallengedList] ==
                gameId,
            "Wrong index."
        );
    }

    // makes sure at least one block passed since the last action
    function noRaceCondition(bytes32 gameId) private view {
        require(
            games[gameId].blockNumLastAction < block.number,
            "Must wait one block."
        );
    }

    constructor() public {
        owner = msg.sender;
        fees = 0;
    }

    // no unregister, since no one would pay for that
    function register(string memory withName) public {
        require(!stringsAreEqual(withName, ""), "Name cannot be blank.");

        if (stringsAreEqual(players[msg.sender], "")) {
            players[msg.sender] = withName;

            emit NewPlayer(msg.sender, withName);
        } else {
            revert("Cannot register twice.");
        }
    }

    function changeName(string memory newName) public onlyRegisteredUsers {
        emit PlayerNameChanged(msg.sender, newName);
        players[msg.sender] = newName;
    }

    function createGame(address payable challenged, bytes32 yourMoveHash)
        public
        payable
        onlyRegisteredUsers
        returns (bytes32 gameId)
    {
        require(msg.sender != challenged, "You cannot challenge yourself");
        require(msg.value > 100, "No ETH, no game.");

        Game memory game;
        game.challenger = msg.sender;
        game.challenged = challenged;
        game.bet = msg.value;
        game.challengerMoveHash = yourMoveHash;

        gameId = computeGameId(
            game.challenger,
            game.challenged,
            game.bet,
            game.challengerMoveHash
        );

        // since (msg.value > 100) when creating a new game, all games must
        // have a non-zero bet
        require(games[gameId].bet == 0, "Game already created.");

        games[gameId] = game;

        challenges[game.challenger].push(gameId);
        challenges[game.challenged].push(gameId);

        emit NewGame(msg.sender, challenged, gameId);
    }

    function acceptChallenge(bytes32 gameId)
        public
        payable
        onlyRegisteredUsers
    {
        onlyExistingGames(gameId);
        onlyChallenged(gameId);

        require(games[gameId].accepted == false, "You can only accept once");

        require(
            games[gameId].bet == msg.value,
            "Please deposit the same amount as your peer."
        );

        games[gameId].accepted = true;
        games[gameId].blockNumLastAction = block.number;

        emit ChallengeAccepted(gameId);
    }

    function makeCounterMove(bytes32 gameId, string memory yourMove)
        public
        onlyRegisteredUsers
    {
        onlyExistingGames(gameId);
        onlyChallenged(gameId);
        onlyValidMoves(yourMove);
        onlyAcceptedChallenges(gameId);

        require(
            stringsAreEqual(games[gameId].challengedMove, ""),
            "You can only move once"
        );

        games[gameId].challengedMove = yourMove;
        games[gameId].blockNumLastAction = block.number;

        emit ChallengedMoved(gameId);
    }

    function resolveChallenge(
        bytes32 gameId,
        string memory yourMoveWithSalt,
        uint256 indexInChallengerList,
        uint256 indexInChallengedList
    ) public {
        onlyExistingGames(gameId);
        onlyChallengesWithCounterMove(gameId);
        onlyChallenger(gameId);
        onlyCorrectIndices(
            gameId,
            indexInChallengerList,
            indexInChallengedList
        );
        noRaceCondition(gameId);

        require(
            hash(yourMoveWithSalt) == games[gameId].challengerMoveHash,
            "Please reveal your real move."
        );

        uint256 bet = games[gameId].bet;

        // percent = bet / 100;
        // fee = percent * 2;
        // collateral = percent * 10;
        // winnings = (bet * 2) - fee - collateral;
        // some of them are substituted in the code below

        uint256 fee = (bet / 100) * 2;
        uint256 winnings = (bet * 2) - ((bet / 100) * 10) - fee;
        string memory crm = firstChar(yourMoveWithSalt);
        onlyValidMoves(crm);
        string memory cdm = firstChar(games[gameId].challengedMove);
        uint256 winner = rps(crm, cdm);
        address payable challenger = games[gameId].challenger;
        address payable challenged = games[gameId].challenged;

        uint256 challengerPayout;
        uint256 challengedPayout;
        uint256 collectedFees;

        //collateral = percent * 10;
        if (winner == 0) {
            // challenger wins
            challengerPayout = winnings;
            challengedPayout = (bet / 100) * 10;
            collectedFees = fee;
        } else if (winner == 1) {
            // challenged wins
            challengerPayout = (bet / 100) * 10;
            challengedPayout = winnings;
            collectedFees = fee;
        } else if (winner == 2) {
            // tie
            challengerPayout = bet;
            challengedPayout = bet;
            collectedFees = 0;
        }

        deleteGame(gameId, indexInChallengerList, indexInChallengedList);
        emit GameResolved(gameId, bet, challenger, challenged, crm, cdm);

        fees += collectedFees;

        // payout last - if it fails, revert() is called -> state will never
        // be messy
        challenger.transfer(challengerPayout);
        challenged.transfer(challengedPayout);
    }

    function withdrawChallenge(
        bytes32 gameId,
        uint256 indexInChallengerList,
        uint256 indexInChallengedList
    ) public onlyRegisteredUsers {
        onlyExistingGames(gameId);
        onlyChallenger(gameId);
        onlyCorrectIndices(
            gameId,
            indexInChallengerList,
            indexInChallengedList
        );
        require(
            !games[gameId].accepted,
            "You cannot withdraw an accepted challenge."
        );

        uint256 bet = games[gameId].bet;

        deleteGame(gameId, indexInChallengerList, indexInChallengedList);

        emit ChallengeWithdrawn(gameId);

        // must be the challenger (s. above)
        msg.sender.transfer(bet);
    }

    function challengedDefaults(
        bytes32 gameId,
        uint256 indexInChallengerList,
        uint256 indexInChallengedList
    ) public {
        onlyExistingGames(gameId);
        onlyAcceptedChallenges(gameId);
        onlyChallenger(gameId);

        require(
            games[gameId].blockNumLastAction < block.number - 20,
            "Challenger still has time to reveal."
        );
        require(
            stringsAreEqual(games[gameId].challengedMove, ""),
            "The challenged has moved."
        );

        uint256 bet = games[gameId].bet;
        address payable payoutAddress = games[gameId].challenger;

        deleteGame(gameId, indexInChallengerList, indexInChallengedList);

        emit Default(gameId);
        payoutAddress.transfer(bet * 2);
    }

    function challengerDefaults(
        bytes32 gameId,
        uint256 indexInChallengerList,
        uint256 indexInChallengedList
    ) public {
        onlyExistingGames(gameId);
        onlyAcceptedChallenges(gameId);
        onlyChallenged(gameId);

        require(
            !stringsAreEqual(games[gameId].challengedMove, ""),
            "Challenged has not moved."
        );

        require(
            games[gameId].blockNumLastAction < block.number - 20,
            "Challenger still has time to reveal."
        );

        uint256 bet = games[gameId].bet;
        address payable payoutAddress = games[gameId].challenged;

        deleteGame(gameId, indexInChallengerList, indexInChallengedList);

        emit Default(gameId);

        payoutAddress.transfer(bet * 2);
    }

    function deleteChallengeFromPlayerList(
        uint256 challengeIndex,
        address playerAddress
    ) private {
        uint256 challengesLength = challenges[playerAddress].length;

        if (challengesLength - 1 != challengeIndex) {
            bytes32 last = challenges[playerAddress][challengesLength - 1];
            challenges[playerAddress][challengeIndex] = last;
        }

        challenges[playerAddress].pop();
    }

    function deleteGame(
        bytes32 gameId,
        uint256 indexInChallengerList,
        uint256 indexInChallengedList
    ) private {
        onlyExistingGames(gameId);
        onlyCorrectIndices(
            gameId,
            indexInChallengerList,
            indexInChallengedList
        );

        deleteChallengeFromPlayerList(
            indexInChallengerList,
            games[gameId].challenger
        );

        deleteChallengeFromPlayerList(
            indexInChallengedList,
            games[gameId].challenged
        );

        delete games[gameId];
    }

    function payoutFees() public {
        require(msg.sender == owner, "Not permitted");
        uint256 feesToSend = 0;
        feesToSend = fees;
        fees = 0;
        owner.transfer(feesToSend);
    }

    // Getters

    function playerIsRegistered(address payable atAddress)
        public
        view
        returns (bool isRegistered)
    {
        return !stringsAreEqual(players[atAddress], "");
    }

    function getDisplayName() public view returns (string memory displayName) {
        return players[msg.sender];
    }

    function getDisplayNameFromAddress(address _a)
        public
        view
        returns (string memory displayName)
    {
        return players[_a];
    }

    function getNumberOfChallenges(address forAddress)
        public
        view
        returns (uint256 numberOfChallenges)
    {
        return challenges[forAddress].length;
    }

    function getGameIdForChallengeWithNumber(uint256 number, address forAddress)
        public
        view
        returns (bytes32 gameId)
    {
        require(
            challenges[forAddress].length > number,
            "No challenge for this number"
        );
        return challenges[forAddress][number];
    }

    function getGameForAddress(address payable playerAddress, uint256 atIndex)
        public
        view
        returns (
            bytes32 gameId,
            address payable challenger,
            address payable challenged,
            uint256 bet,
            bool accepted,
            string memory challengedMove,
            uint256 blockNumLastAction
        )
    {
        gameId = challenges[playerAddress][atIndex];

        (
            challenger,
            challenged,
            bet,
            accepted,
            challengedMove,
            blockNumLastAction
        ) = getGameById(gameId);
    }

    function getGameById(bytes32 gameId)
        public
        view
        returns (
            address payable challenger,
            address payable challenged,
            uint256 bet,
            bool accepted,
            string memory challengedMove,
            uint256 blockNumLastAction
        )
    {
        Game memory game = games[gameId];

        challenger = game.challenger;
        challenged = game.challenged;
        bet = game.bet;
        accepted = game.accepted;
        challengedMove = game.challengedMove;
        blockNumLastAction = game.blockNumLastAction;
    }

    function getOwner() public view returns (address payable ownerAddress) {
        return owner;
    }

    function getFees() public view returns (uint256 feesInWei) {
        return fees;
    }

    // Pure functions

    function computeGameId(
        address payable challenger,
        address payable challenged,
        uint256 bet,
        bytes32 moveHash
    ) public pure returns (bytes32 gameId) {
        gameId = keccak256(
            abi.encodePacked(challenger, challenged, moveHash, bet)
        );

        return gameId;
    }

    function firstChar(string memory _s) private pure returns (string memory) {
        bytes memory asBytest = bytes(_s);
        bytes memory fc = new bytes(1);

        fc[0] = asBytest[0];
        return string(fc);
    }

    function stringsAreEqual(string memory a, string memory b)
        private
        pure
        returns (bool)
    {
        return hash(a) == hash(b);
    }

    function hash(string memory _s) private pure returns (bytes32) {
        return keccak256(abi.encodePacked(_s));
    }

    function rps(string memory p1, string memory p2)
        private
        pure
        returns (uint256 winner)
    {
        onlyValidMoves(p1);
        onlyValidMoves(p2);

        if (stringsAreEqual(p1, p2)) {
            return 2;
        }

        string memory concatenated = string(abi.encodePacked(p1, p2));

        if (stringsAreEqual(concatenated, "rp")) return 1;
        if (stringsAreEqual(concatenated, "rs")) return 0;
        if (stringsAreEqual(concatenated, "pr")) return 0;
        if (stringsAreEqual(concatenated, "ps")) return 1;
        if (stringsAreEqual(concatenated, "sr")) return 1;
        if (stringsAreEqual(concatenated, "sp")) return 0;
    }
}
